using System;
using System.Collections.Generic;


namespace interp
{
public class MainFunction : ICaller {
    private  Function decl;
    public MainFunction(Function declaration) {
        this.decl = declaration;
    }

    public  int argumentNum() {
        return decl.parameters.Count;
    }
    public override  String ToString() {
        return "<fn " + decl.name.lexeme + ">";
    }


      object ICaller.call(Interpreter interpreter, List<Object> arguments) {
        Setting environment = new Setting(interpreter.globals);
        for (int i = 0; i < decl.parameters.Count; i++) {
            environment.define(decl.parameters[i].lexeme, arguments[i]);
        }
        try {
            interpreter.executeBlock(decl.body, environment);
        } catch (Exception returnValue) {
            return null;
        }
        return null;
    }

       
    }
}