using System;
using System.Collections.Generic;

namespace interp
{
public class Scanner {
    private  String source;
    private  List<Token> tokens = new List<Token>();
    private int start = 0;
    private int curr = 0;
    private int line = 1;
    private  Dictionary<String, TypeOfToken?> keywords;

    public Scanner(String source) {
        this.source = source;
        keywords = new Dictionary<string, TypeOfToken?>();
        keywords.Add("else",   TypeOfToken.ELSE);
        keywords.Add("false",  TypeOfToken.FALSE);
        keywords.Add("func", TypeOfToken.FUNC);
        keywords.Add("if",     TypeOfToken.IF);
        keywords.Add("nil",    TypeOfToken.NIL);
        keywords.Add("or",     TypeOfToken.OR);
        keywords.Add("print",  TypeOfToken.PRINT);
        keywords.Add("return", TypeOfToken.RETURN);
        keywords.Add("true",   TypeOfToken.TRUE);
        keywords.Add("var",    TypeOfToken.VAR);
        keywords.Add("while",  TypeOfToken.WHILE);
    }

    public List<Token> scanTokens() {
        while (!isAtEnd()) {
            start = curr;
            scanToken();
        }

        tokens.Add(new Token(TypeOfToken.EOF, "", null, line));
        return tokens;
    }

    private bool isAtEnd() {
        return curr >= source.Length;
    }

    private void scanToken() {
        char ch = advance();
        Console.WriteLine("Char " + ch);
        switch (ch) {
            case '(':
                addToken(TypeOfToken.LEFT_PAREN);
                break;
            case ')':
                addToken(TypeOfToken.RIGHT_PAREN);
                break;
            case '{':
                addToken(TypeOfToken.LEFT_BRACE);
                break;
            case '}':
                addToken(TypeOfToken.RIGHT_BRACE);
                break;
            case ',':
                addToken(TypeOfToken.COMMA);
                break;
            case '.':
                addToken(TypeOfToken.DOT);
                break;
            case '-':
                addToken(TypeOfToken.MINUS);
                break;
            case '+':
                addToken(TypeOfToken.PLUS);
                break;
            case ';':
                addToken(TypeOfToken.SEMICOLON);
                break;
            case '*':
                addToken(TypeOfToken.STAR);
                break;
            case '!':
                addToken(match('=') ? TypeOfToken.EXCL_EQUAL : TypeOfToken.EXCL);
                break;
            case '=':
                addToken(match('=') ? TypeOfToken.EQUAL_EQUAL : TypeOfToken.EQUAL);
                break;
            case '<':
                addToken(match('=') ? TypeOfToken.LESS_EQUAL : TypeOfToken.LESS);
                break;
            case '>':
                addToken(match('=') ? TypeOfToken.GREATER_EQUAL : TypeOfToken.GREATER);
                break;
            case '/':
                if (match('/')) {
                    while (peek() != '\n' && !isAtEnd()) advance();
                } else {
                    addToken(TypeOfToken.SLASH);
                }
                break;
            case ' ':
            case '\r':
            case '\t':
                break;

            case '\n':
                line++;
                break;

            case '"':
                getString();
                break;

            default:
                if (isDigit(ch)) {
                    number();
                } else if (isAlpha(ch)) {
                    identifier();
                }
                else {
                    Program.error("Невідомий символ.");
                }
                break;
        }
    }

    private char advance() {
        curr++;
        return source[curr - 1];
    }

    private void addToken(TypeOfToken type) {
        addToken(type, null);
    }

    private void addToken(TypeOfToken type, Object literal) {
        int length = curr - start;
        Console.WriteLine("start " + start + " curr " + curr + " lenght " + length);
        if(length > 0)
        {
        String text = source.Substring(start, curr - start);
        tokens.Add(new Token(type, text, literal, line));
            
        }
    }

    private bool match(char expected) {
        if (isAtEnd()) return false;
        if (source[curr] != expected) return false;

        curr++;
        return true;
    }

    private char peek() {
        if (isAtEnd()) return '\0';
        return source[curr];
    }


    private bool isDigit(char ch) {
        return ch >= '0' && ch <= '9';
    }

    private void number() {
        while (isDigit(peek())) advance();

        if (peek() == '.' && isDigit(peekNext())) {
            advance();

            while (isDigit(peek())) advance();
        }

        addToken(TypeOfToken.NUMBER, Double.Parse(source.Substring(start, curr - start)));
    }

    private char peekNext() {
        if (curr + 1 >= source.Length) return '\0';
        return source[curr + 1];
    }

    private void identifier() {
        while (isAlphaNum(peek())) advance();

        string text = source.Substring(start, curr - start);

        TypeOfToken? type = keywords.GetValueOrDefault(text);
        Console.WriteLine("Type " + type);
        if (type == null) type = TypeOfToken.IDENTIFIER;
        addToken((TypeOfToken)type);
    }

    private bool isAlpha(char c) {
        return (c >= 'a' && c <= 'z') ||
                (c >= 'A' && c <= 'Z') ||
                c == '_';
    }

    private bool isAlphaNum(char ch) {
        return isAlpha(ch) || isDigit(ch);
    }


    private void getString() {
        while (peek() != '"' && !isAtEnd()) {
            if (peek() == '\n') line++;
            advance();
        }

        if (isAtEnd()) {
            Program.error("Незакінчений рядок.");
            return;
        }

        advance();

        String value = source.Substring(start, curr - start -1 );
        addToken(TypeOfToken.STRING, value);
    }
}
}