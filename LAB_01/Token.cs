using System;
using System.Collections.Generic;

namespace interp
{
public class Token {
    public TypeOfToken tokenType{ get; set; }
    public String lexeme{ get; set; }
    public Object literal{ get; set; }
    public int line { get; set; }

    public Token(TypeOfToken tokenType, String lexeme, Object literal, int line) {
        this.tokenType = tokenType;
        this.lexeme = lexeme;
        this.literal = literal;
        this.line = line;
    }
    public String toString() {
        return tokenType + " " + lexeme + " " + literal;
    }
}
    public enum TypeOfToken
    {
        LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE,
        COMMA, DOT, MINUS, PLUS, SEMICOLON, SLASH, STAR,

        EXCL, EXCL_EQUAL,
        EQUAL, EQUAL_EQUAL,
        GREATER, GREATER_EQUAL,
        LESS, LESS_EQUAL,

        IDENTIFIER, STRING, NUMBER,

        ELSE, FALSE, FUNC, IF, NIL, OR,
        PRINT, RETURN, TRUE, VAR, WHILE,
        EOF
    }
}