using System.Collections.Generic;

namespace interp
{
 public abstract class Expression_ {

public abstract T accept<T>(Client<T> visitor);

  public interface Client<T> {
     T visitAssignExpression(Assignment expression);
     T visitBinaryExpression(Binary expression);
     T visitCallExpression(Call expression);
     T visitGroupingExpression(Grouping expression);
    T visitLiteralExpression(Literal expression);
    T visitUnaryExpression(Unary expression);
    T visitVariableExpression(Variable expression);
  }

}
 public class Assignment : Expression_ {
    public Assignment(Token name, Expression_ value) {
      this.name = name;
      this.value = value;
    }

    public override T accept<T>(Client<T> visitor) {
      return visitor.visitAssignExpression(this);
    }

    public Token name{ get; set; }
    public Expression_ value{ get; set; }
  }
  public class Binary : Expression_ {
    public Binary(Expression_ left, Token operande, Expression_ right) {
      this.left = left;
      this.operande = operande;
      this.right = right;
    }

  
    public override T accept<T>(Client<T> client) {
      return client.visitBinaryExpression(this);
    }

    public Expression_ left{ get; set; }
    public Token operande{ get; set; }
    public Expression_ right{ get; set; }
  }
public class Call : Expression_ {
    public Call(Expression_ callIn, Token paren, List<Expression_> arguments) {
      this.callIn = callIn;
      this.paren = paren;
      this.arguments = arguments;
    }
    public override T accept<T>(Client<T> visitor) {
      return visitor.visitCallExpression(this);
    }

    public Expression_ callIn{ get; set; }
    public Token paren{ get; set; }
    public  List<Expression_> arguments{ get; set; }
  }
public class Grouping : Expression_ {
    public Grouping(Expression_ expression) {
      this.expression = expression;
    }

    public override T accept<T>(Client<T> client) {
      return client.visitGroupingExpression(this);
    }

    public Expression_ expression{ get; set; }
  }
public class Literal :  Expression_ {
    public Literal(object value) {
      this.value = value;
    }

    public override T accept<T>(Client<T> client) {
      return client.visitLiteralExpression(this);
    }

    public object value{ get; set; }
  }
public class Unary : Expression_ {
    public Unary(Token operande, Expression_ right) {
      this.operande = operande;
      this.right = right;
    }

   public override T accept<T>(Client<T> client) {
      return client.visitUnaryExpression(this);
    }

    public Token operande{ get; set; }
    public Expression_ right{ get; set; }
  }
public class Variable :  Expression_ {
    public Variable(Token name) {
      this.name = name;
    }

   public override T accept<T>(Client<T> client) {
      return client.visitVariableExpression(this);
    }

    public Token name{ get; set; }
  }

}