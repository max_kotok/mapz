using System;
using System.Collections.Generic;

namespace interp
{
class Setting {
    Setting close = null;
    private Dictionary<string, object> values = new Dictionary<string, object>();
    public Setting() {
       
    }

    public Setting(Setting close) {
        this.close = close;
    }
    public Object get(Token name) {
        if (values.ContainsKey(name.lexeme)) {
            return values.GetValueOrDefault(name.lexeme);
        }

        if (close != null) return close.get(name);
           throw new Exception("Нерозпізнана змінна '" + name.lexeme + "'.");
    }

  public   void assign(Token name, Object value) {
        if (!values.ContainsKey(name.lexeme)) {
            values.Add(name.lexeme, value);
        } else {
            values[name.lexeme] = value;
        }
        if (close != null) {
            close.assign(name, value);
            return;
        }
    }

    public void define(String name, object value) {
        if(values.ContainsKey(name))
        values[name] = value;
        else
        values.Add(name, value);
    }
}
}