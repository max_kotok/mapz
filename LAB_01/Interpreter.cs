using System.Collections.Generic;
using System;
using interp;
using System.IO;
using System.Text;


namespace interp
{
class Interpreter : Expression_.Client<Object>, Statement.Client<Object> {
    public interp.Setting globals = new Setting();

        public class sin_ : ICaller
        {
            public int argumentNum()
            {
               return 1;
            }

            public object call(Interpreter interpreter, List<object> arguments)
            {
                return Math.Sin((double)arguments[0]);
            }

            public String toString() { return "<вбудована функція sin>"; }
        }

          public class cos_ : ICaller
        {
            public int argumentNum()
            {
               return 1;
            }

            public object call(Interpreter interpreter, List<object> arguments)
            {
                return Math.Cos((double)arguments[0]);
            }

        
            public String toString() { return "<вбудована функція cos>"; }
        }

         public class pow_ : ICaller
        {
            public int argumentNum()
            {
               return 2;
            }
            public Object call(Interpreter interpreter, List<Object> arguments) {
               return Math.Pow((double)arguments[0], (double)arguments[1]);
            }

             public string toString() { return "<вбудована функція pow>"; }
        }



    public class sqrt_ : ICaller
        {
            public int argumentNum()
            {
               return 1;
            }
            public Object call(Interpreter interpreter, List<Object> arguments) {
               return Math.Sqrt((double)arguments[0]);
            }

             public string toString() { return "<вбудована функція pow>"; }
        }



    public class sqr_ : ICaller
        {
            public int argumentNum()
            {
               return 1;
            }
            public Object call(Interpreter interpreter, List<Object> arguments) {
               return Math.Pow((double)arguments[0],2);
            }

             public string toString() { return "<вбудована функція kvadrat>"; }
        }

        public class dLn_ : ICaller
        {
            public int argumentNum()
            {
               return 1;
            }
            public Object call(Interpreter interpreter, List<Object> arguments) {
               return 1/Math.Log((double)arguments[0]);
            }

             public string toString() {return "<вбудована функція differentiatePolynomial>"; }
        }


        public class dLn_2 : ICaller
        {
            public int argumentNum()
            {
               return 1;
            }
            public Object call(Interpreter interpreter, List<Object> arguments) {
                return 1/Math.Log((double)arguments[0]);
            }

             public string toString() {return "<вбудована функція differentiatePolynomial>"; }
        }

         public class dMnogochlen : ICaller
        {
            public int argumentNum()
            {
               return 2;
            }
            public Object call(Interpreter interpreter, List<Object> arguments) {
                double answer = 0;
                String nextChar = "+";
                String formula = (String)arguments[0];
                String[] parts = formula.Split(" ");
                foreach (String part in parts) {
                    if (part != "+"&& part !=  "-") {
                        StringBuilder coeffStr = new StringBuilder();
                        int i;
                        for (i = 0; part[i] != 'x'; i++)
                            coeffStr.Append(part[i]);
                        

                        int koef = Int32.Parse(coeffStr.ToString().Replace("\"", ""));
                        StringBuilder powStr = new StringBuilder();
                        for (i = i + 2; i != part.Length; i++)
                            powStr.Append(part[i]);

                        int power = Int32.Parse(powStr.ToString().Replace("\"", ""));
                        if (nextChar == "+")
                            answer += koef * power * Math.Pow((double) arguments[1], power - 1);
                        else
                            answer -= koef * power * Math.Pow((double) arguments[1], power - 1);
                    }
                    if (part == "+") {
                        nextChar = "+";
                    } else if (part == "-") {
                        nextChar = "-";
                    }
                }
                return answer;            }

             public string toString() {return "<вбудована функція differentiatePolynomial>"; }
        }


        public Interpreter() {
            globals.define("sin_", new sin_()); 
            globals.define("cos_", new cos_()); 
            globals.define("pow_", new pow_()); 
            globals.define("sqrt_", new sqrt_()); 
            globals.define("sqr_", new sqr_()); 
            globals.define("dLn_", new dLn_()); 
            globals.define("dLn_2", new dLn_2()); 
            globals.define("dMnogochlen", new dMnogochlen()); 
        }
       
   public void interpretate(List<Statement> stats) {
        try {
            foreach (Statement statements in stats) {
                execute(statements);
            }
        } catch (Exception error) {
            Program.runtimeError(error);
        }
    }

private void execute(Statement statement) => statement.accept(this);

public void executeBlock(List<Statement> statements, interp.Setting setting) {
        interp.Setting previous = this.globals;
        try {
            this.globals = setting;

            foreach (Statement statement in statements) {
                execute(statement);
            }
        } finally {
            this.globals = previous;
        }
    }

    public object goToBlockStatement(Block statement) {
        executeBlock(statement.statements, new interp.Setting(this.globals));
        return null;
    }

    public  object visitLiteralExpression(Literal expression) {
        return expression.value;
    }

    public object visitUnaryExpression(Unary expression) {
        Object right = evaluate(expression.right);

        switch (expression.operande.tokenType) {
            case TypeOfToken.EXCL:
                return !isTruth(right);
            case TypeOfToken.MINUS:
                checkNumberOperand(expression.operande, right);
                return -(double)right;
        }

        return null;
    }
    public  object visitVariableExpression(Variable expr) {
        return globals.get(expr.name);
    }

    private void checkNumberOperand(Token operande, Object operand) {
        if (operande is Double) return;
    }

    private void checkOperandNum(Token operande, Object left, Object right) {
        if (left is Double && right is Double) return;

        throw new Exception("Операнди повинні бути числами.");
    }

    private bool isTruth(Object obj) {
        if (obj == null) return false;
        if (obj is Boolean) return (Boolean)obj;
        return true;
    }

    private bool isEqual(Object a, Object b) {
        if (a == null && b == null) return true;
        if (a == null) return false;

        return a == b;
    }

    private String stringify(Object object_) {
        if (object_ == null) return "nil";

        if (object_ is Double) {
            String text = object_.ToString();
            if (text.EndsWith(".0")) {
                text = text.Substring(0, text.Length - 2);
            }
            return text;
        }

        return object_.ToString();
    }

    
    public object visitGroupingExpression(Grouping expression) {
        return evaluate(expression.expression);
    }

    private Object evaluate(Expression_ expression) {
        return expression.accept(this);
    }

    public object goToExpressionStatement(Expression statement) {
        evaluate(statement.expression);
        return null;
    }

    public  object goToFunctionStatement(Function statement) {
        MainFunction function = new MainFunction(statement);
        globals.define(statement.name.lexeme, function);
        return null;
    }

    public  object goToIfStatement(If statement) {
        if (isTruth(evaluate(statement.condition))) {
            execute(statement.thenBranch);
        } else if (statement.elseBranch != null) {
            execute(statement.elseBranch);
        }
        return null;
    }

    public object goToPrintStatement(Print statement) {
        Object value = evaluate(statement.expression);
        Console.WriteLine(stringify(value));
        return null;
    }

    public object goToReturnStatement(Return statement) {
        Object value = null;
        if (statement.value != null) value = evaluate(statement.value);

        return null;
    }

    public object goToVarStatement(Var statement) {
        Object value = null;
        if (statement.initializer != null) {
            value = evaluate(statement.initializer);
        }

        globals.define(statement.name.lexeme, value);
        return null;
    }

    public object goToWhileStatement(While statement) {
        while (isTruth(evaluate(statement.condition))) {
            execute(statement.body);
        }
        return null;
    }

    public object visitAssignExpression(Assignment expression) {
        Object value = evaluate(expression.value);

        globals.assign(expression.name, value);
        return value;
    }

    public object visitBinaryExpression(Binary expression) {
        Object left = evaluate(expression.left);
        Object right = evaluate(expression.right);

        switch (expression.operande.tokenType) {
            case TypeOfToken.GREATER:
                checkOperandNum(expression.operande, left, right);
                return (double)left > (double)right;
            case TypeOfToken.GREATER_EQUAL:
                checkOperandNum(expression.operande, left, right);
                return (double)left >= (double)right;
            case TypeOfToken.LESS:
                checkOperandNum(expression.operande, left, right);
                return (double)left < (double)right;
            case TypeOfToken.LESS_EQUAL:
                checkOperandNum(expression.operande, left, right);
                return (double)left <= (double)right;
            case TypeOfToken.EXCL_EQUAL: return !isEqual(left, right);
            case TypeOfToken.EQUAL_EQUAL: return isEqual(left, right);
            case TypeOfToken.MINUS:
                checkOperandNum(expression.operande, left, right);
                return (double)left - (double)right;
            case TypeOfToken.PLUS:
                if (left is Double && right is Double) {
                    return (double)left + (double)right;
                }

                if (left is String && right is String) {
                    return (String)left + (String)right;
                }

                return null;

            case TypeOfToken.SLASH:
                checkOperandNum(expression.operande, left, right);
                return (double)left / (double)right;
            case TypeOfToken.STAR:
                checkOperandNum(expression.operande, left, right);
                return (double)left * (double)right;
        }

        return null;
    }

    public object visitCallExpression(Call expression) {
        Object callIn = evaluate(expression.callIn);

        List<Object> arguments = new List<Object>();
        foreach (Expression_ argument in expression.arguments) {
            arguments.Add(evaluate(argument));
        }

        if (!(callIn is ICaller)) {
            throw new Exception("Викликати можливо лише функції.");
        }

        ICaller function = (ICaller)callIn;

        if (arguments.Count != function.argumentNum()) {
            throw new Exception("Очікується " +
                    function.argumentNum() + " аргументів але отримано " +
                    arguments.Count + ".");
        }
        return function.call(this, arguments);
    }

    public List<Statement> optimize(List<Statement> statements)  {
        List<Statement> optimizatedList = new List<Statement>();
        PrintTree aPrinter = new PrintTree();
        TextWriter aFile = File.CreateText("text.txt");
        foreach (Statement statement in statements) {
            if (statement is While) {
                While temp = (While)statement;
                if (temp.condition is Literal) {
                    if (!(bool) temp.condition.accept(this)) {
                        aFile.WriteLine("\nBefore: ");
                        aFile.WriteLine(aPrinter.print(statement));
                        aFile.WriteLine("\nAfter: ");
                        aFile.WriteLine("Statement was removed (loop body will never execute)\n");
                        aFile.WriteLine("/**************************************************************************/");
                        continue;
                    }
                }
                else if (temp.body is Block) {
                    aFile.WriteLine("\nBefore: ");
                    aFile.WriteLine(aPrinter.print(statement));
                    aFile.WriteLine("\nAfter: ");
                    Block tempBlock = (Block) temp.body;
                    if (tempBlock.statements.Count == 0) {
                        aFile.WriteLine("Statement was removed (loop body is empty)\n");
                        aFile.WriteLine("/**************************************************************************/");
                    } else if (tempBlock.statements.Count == 1) {
                        While newLoop = new While(temp.condition, tempBlock.statements[0]);
                        optimizatedList.Add(newLoop);
                        aFile.WriteLine(aPrinter.print(newLoop));
                        aFile.WriteLine("/**************************************************************************/");
                    } else {
                        optimizatedList.Add(statement);
                    }
                }
            }
            else if (statement is If) {
                If temp = (If)statement;
                aFile.WriteLine("\nBefore: ");
                aFile.WriteLine(aPrinter.print(statement));
                aFile.WriteLine("\nAfter: ");
                if ((bool)temp.condition.accept(this)) {
                    optimizatedList.Add(temp.thenBranch);
                    aFile.WriteLine(aPrinter.print(temp.thenBranch));
                }
                else if (temp.elseBranch != null) {
                    optimizatedList.Add(temp.elseBranch);
                    aFile.WriteLine(aPrinter.print(temp.elseBranch));
                }
                else {
                    aFile.WriteLine("Statement was removed (condition is false and no else block provided)\n");
                }
                aFile.WriteLine("/**************************************************************************/");
            }
            else {
                optimizatedList.Add(statement);
            }
        }
        aFile.Close();
        return optimizatedList;
    }
        
    }

}

