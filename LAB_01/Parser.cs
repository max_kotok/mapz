using System;
using System.Collections.Generic;
using interp;
using System.IO;


namespace interp
{
    class Parser {

    private  List<Token> tokens;
    private int current = 0;

    public Parser(List<Token> tokens) {
        this.tokens = tokens;
        foreach(Token token in tokens)
            Console.WriteLine("Token " + token.toString());
    }

    public List<Statement> parse()  {
        List<Statement> statements = new List<Statement>();
        Dictionary <string, Statement> tempStatements = new Dictionary<string, Statement>();
        PrintTree aPrinter = new PrintTree();
        TextWriter aFile = File.CreateText("text.txt");
        while (!isAtEnd()) {
            Statement statement = declaration();
            if(statement == null)
            continue;
            Console.WriteLine(statement);
            if(statement is While)
            {
               While temp =  ((While)statement);
               Console.WriteLine("Condition " + temp.condition  + " statement " + temp.body.ToString());
               if(temp.body is Block)
               {
               foreach(Statement statement_ in ((Block)temp.body).statements)
               {
                    Console.WriteLine("Statement " + statement_);
               }
               }
            
            }
            aFile.WriteLine(aPrinter.print(statement));
            String name;
            if (statement is Var) {
                Var variable = (Var) statement;
                name = variable.name.lexeme;
                tempStatements.Add(name, statement);
                foreach (String key in tempStatements.Keys) {
                    statements.Add(tempStatements.GetValueOrDefault(key));
                }
            } else if (statement is Expression) {
                Expression expr = (Expression) statement;
                if (expr.expression is Assignment) {
                    Assignment assign = (Assignment) expr.expression;
                    name = assign.name.lexeme;
                    if(!tempStatements.ContainsKey(name))
                    tempStatements.Add(name, statement);
                    else
                    tempStatements[name] = statement;
                    foreach (String key in tempStatements.Keys) {
                        statements.Add(tempStatements.GetValueOrDefault(key));
                    }
                }
            } else {
                statements.Add(statement);
            }
        }
        aFile.Close();
        return statements;
    }

    private Expression_ expression() {
        return assignment();
    }

    private Statement declaration() {
        try {
            if (match(TypeOfToken.FUNC)) return function();
            if (match(TypeOfToken.VAR)) return varDeclaration();

            return statement();
        } catch (Exception e) {
            synchronize();
            return null;
        }
    }

    private Statement statement() {
        if (match(TypeOfToken.IF)) return ifStatement();
        if (match(TypeOfToken.PRINT)) return printStatement();
        if (match(TypeOfToken.RETURN)) return returnStatement();
        if (match(TypeOfToken.WHILE)) return whileStatement();
        if (match(TypeOfToken.LEFT_BRACE)) return new Block(block());
        return expressionStatement();
    }

    private Statement ifStatement() {
        consume(TypeOfToken.LEFT_PAREN, "Очікується '(' після 'if'.");
        Expression_ condition = expression();
        consume(TypeOfToken.RIGHT_PAREN, "Очікується ')' наприкінці умови умовного оператора.");

        Statement thenBranch = statement();
        Statement elseBranch = null;
        if (match(TypeOfToken.ELSE)) {
            elseBranch = statement();
        }

        return new If(condition, thenBranch, elseBranch);
    }

    private Statement printStatement() {
        Expression_ value = expression();
        consume(TypeOfToken.SEMICOLON, "Очікується ';' наприкінці виразу.");
        return new Print(value);
    }

    private Statement returnStatement() {
        Token keyword = previous();
        Expression_ value = null;
        if (!check(TypeOfToken.SEMICOLON)) {
            value = expression();
        }

        consume(TypeOfToken.SEMICOLON, "Очікується ';' після виразу повернення.");
        return new Return(keyword, value);
    }

    private Statement varDeclaration() {
        Token name = consume(TypeOfToken.IDENTIFIER, "Очікується ім'я змінної.");

        Expression_ initializer = null;
        if (match(TypeOfToken.EQUAL)) {
            initializer = expression();
        }

        consume(TypeOfToken.SEMICOLON, "Очікується ';' після оголошення змінної.");
        return new Var(name, initializer);
    }

    private Statement whileStatement() {
        consume(TypeOfToken.LEFT_PAREN, "Очікується '(' після 'while'.");
        Expression_ condition = expression();
        consume(TypeOfToken.RIGHT_PAREN, "Очікується ')' після умови циклу.");
        Statement body = statement();

        return new While(condition, body);
    }

    private Statement expressionStatement() {
        Expression_ expr = expression();
        consume(TypeOfToken.SEMICOLON, "Відсутній символ ';' наприкінці виразу.");
        return new Expression(expr);
    }

    private Function function() {
        Token name = consume(TypeOfToken.IDENTIFIER, "Очікується ім'я функції.");
        consume(TypeOfToken.LEFT_PAREN, "Очікується '(' після імені фукнції.");
        List<Token> parameters = new List<Token>();
        if (!check(TypeOfToken.RIGHT_PAREN)) {
            do {
                parameters.Add(consume(TypeOfToken.IDENTIFIER, "Очікується ім'я параметру."));
            } while (match(TypeOfToken.COMMA));
        }
        consume(TypeOfToken.RIGHT_PAREN, "Очікується ')' в кінці списку параметрів.");
        consume(TypeOfToken.LEFT_BRACE, "Очікується '{' перед тілом функції.");
        List<Statement> body = block();
        name.lexeme += "_" + parameters.Count;
        return new Function(name, parameters, body);
    }

    private List<Statement> block() {
        List<Statement> statements = new List<Statement>();

        while (!check(TypeOfToken.RIGHT_BRACE) && !isAtEnd()) {
            statements.Add(declaration());
        }

        consume(TypeOfToken.RIGHT_BRACE, "Відстутня '}' в кінці блоку.");
        return statements;
    }

    private Expression_ assignment() {
        Expression_ expression = equality();

        if (match(TypeOfToken.EQUAL)) {
            Token equals = previous();
            Expression_ value = assignment();

            if (expression is Variable) {
                Token name = ((Variable)expression).name;
                return new Assignment(name, value);
            }

            error(equals, "Неможливо виконати присвоєння.");
        }

        return expression;
    }

    private Expression_ equality() {
        Expression_ expression = comparison();

        while (match(TypeOfToken.EXCL_EQUAL, TypeOfToken.EQUAL_EQUAL)) {
            Token oper = previous();
            Expression_ right = comparison();
            expression = new Binary(expression, oper, right);
        }

        return expression;
    }

    private bool match(params TypeOfToken[] types) {
        foreach (TypeOfToken type in types) {
            if (check(type)) {
                advance();
                return true;
            }
        }

        return false;
    }

    private Token consume(TypeOfToken type, String message) {
        if (check(type)) return advance();

         throw error(peek(), message);
    }

    private bool check(TypeOfToken type) {
        if (isAtEnd()) return false;
        return peek().tokenType == type;
    }

    private Token advance() {
        if (!isAtEnd()) current++;
        return previous();
    }

    private bool isAtEnd() {
        return peek().tokenType == TypeOfToken.EOF;
    }

    private Token peek() {
        return tokens[current];
    }

    private Token previous() {
        return tokens[current - 1];
    }

    private Exception error(Token token, String message) {
        Program.error(token, message);
        return new Exception();
    }

    private void synchronize() {
        advance();

        while (!isAtEnd()) {
            if (previous().tokenType == TypeOfToken.SEMICOLON) return;

            switch (peek().tokenType) {
                case TypeOfToken.FUNC:
                case TypeOfToken.VAR:
                case TypeOfToken.IF:
                case TypeOfToken.WHILE:
                case TypeOfToken.PRINT:
                case TypeOfToken.RETURN:
                    return;
            }

            advance();
        }
    }

    private Expression_ comparison() {
        Expression_ expression = addition();

        while (match(TypeOfToken.GREATER, TypeOfToken.GREATER_EQUAL, TypeOfToken.LESS, TypeOfToken.LESS_EQUAL)) {
            Token operande = previous();
            Expression_ right = addition();
            expression = new Binary(expression, operande, right);
        }

        return expression;
    }

    private Expression_ addition() {
        Expression_ expression = multiplication();

        while (match(TypeOfToken.MINUS, TypeOfToken.PLUS)) {
            Token operande = previous();
            Expression_ right = multiplication();
            expression = new Binary(expression, operande, right);
        }

        return expression;
    }

    private Expression_ multiplication() {
        Expression_ expression = unary();

        while (match(TypeOfToken.SLASH, TypeOfToken.STAR)) {
            Token operande = previous();
            Expression_ right = unary();
            expression = new Binary(expression, operande, right);
        }

        return expression;
    }

    private Expression_ unary() {
        if (match(TypeOfToken.EXCL, TypeOfToken.MINUS)) {
            Token operande = previous();
            Expression_ right = unary();
            return new Unary(operande, right);
        }

        return call();
    }

    private Expression_ call() {
        Expression_ expression = primary();

        while (match(TypeOfToken.LEFT_PAREN)) {
                expression = finishCall(expression);
        }

        return expression;
    }

    private Expression_ finishCall(Expression_ callIn) {
        List<Expression_> arguments = new List<Expression_>();
        if (!check(TypeOfToken.RIGHT_PAREN)) {
            do {
                arguments.Add(expression());
            } while (match(TypeOfToken.COMMA));
        }

        Token paren = consume(TypeOfToken.RIGHT_PAREN, "Очікується ')' після виразу.");
        Variable caller = (Variable)callIn;
        caller.name.lexeme += "_" + arguments.Count;
        return new Call(callIn, paren, arguments);
    }

    private Expression_ primary() {
        if (match(TypeOfToken.FALSE)) return new Literal(false);
        if (match(TypeOfToken.TRUE)) return new Literal(true);
        if (match(TypeOfToken.NIL)) return new Literal(null);

        if (match(TypeOfToken.NUMBER, TypeOfToken.STRING)) {
            return new Literal(previous().literal);
        }

        if (match(TypeOfToken.IDENTIFIER)) {
            return new Variable(previous());
        }

        if (match(TypeOfToken.LEFT_PAREN)) {
            Expression_ expression_ = expression();
            consume(TypeOfToken.RIGHT_PAREN, "Очікується ')' після виразу.");
            return new Grouping(expression_);
        }

        throw error(peek(), "Очікується вираз.");
    }
}
}