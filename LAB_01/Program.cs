﻿using System;
using System.IO;
using System.Collections.Generic;

namespace interp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Запуск...");

              if (args.Length > 1) {
            Console.Write("Запускайте без параметрів!");
        } else if (args.Length == 1) {
            runFile(args[0]);
        } else {
            runPrompt();
        }
        }

         private static  Interpreter interpreter = new Interpreter();
    static bool wasError = false;
    static bool wasRuntimeError = false;



    private static void runFile(String path) {
        run(File.ReadAllText(path));
        if (wasError) return;
        if (wasRuntimeError) return;
    }

    private static void runPrompt()  {
    
        for (;;) {
            try{
            Console.WriteLine("> ");
            run(Console.ReadLine());
            wasError = false;
            } catch(Exception e)
            {
                if(!wasError)
                Console.WriteLine(e.ToString());
                wasError = true;
            }
        }
    }

    private static void run(String source) {
        Scanner scanner = new Scanner(source);
        List<Token> tokens = scanner.scanTokens();

        Parser parser = new Parser(tokens);
        List<Statement> statements = parser.parse();

        if (wasError) return;
        foreach (Statement statement in statements) {
            Console.WriteLine(new PrintTree().print(statement));
        }
        Console.WriteLine("..................................");
        List<Statement> optimizedList = interpreter.optimize(statements);
        foreach (Statement statement in optimizedList) {
            Console.WriteLine(new PrintTree().print(statement));
        }
        interpreter.interpretate(optimizedList);
    }

    static void error(int line, String msg) {
        report(line, "", msg);
    }

    private static void report(int line, String where, String message) {
        Console.WriteLine("[Рядок " + line + "] Помилка" + where + ": " + message);
        wasError = true;
    }

   public static void error(Token token, String msg) {
        if (token.tokenType == TypeOfToken.EOF) {
            report(token.line, " в кінці файлу", msg);
        } else {
            report(token.line, " в '" + token.lexeme + "'", msg);
        }
    }

    public static void error(String msg) {
       Console.WriteLine(msg);
    }

    public static void runtimeError(Exception error) {
        Console.WriteLine(error.ToString());
        wasRuntimeError = true;
    }
    }
}
