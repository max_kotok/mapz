using System;
using System.Text;

namespace interp
{
    class Printer : Expression_.Client<string>, Statement.Client<string>
     {
    string print(Statement statement) {
        return statement.accept(this);
    }

    public string visitAssignExpression(Assignment expression) {
        return parentSize2("=", expression.name.lexeme, expression.value);
    }

    public string visitBinaryExpression(Binary expression) {
        return parentSize(expression.operande.lexeme, expression.left, expression.right);
    }

    public string visitCallExpression(Call expression) {
        return parentSize2("call", expression.callIn);
    }

    public string visitGroupingExpression(Grouping expression) {
        return parentSize("group", expression.expression);
    }

    public string visitLiteralExpression(Literal expression) {
        if (expression.value == null) return "null";
        return expression.value.ToString();
    }

    public string visitUnaryExpression(Unary expression) {
        return parentSize(expression.operande.lexeme, expression.right);
    }

    public String visitVariableExpression(Variable expression) {
        return expression.name.lexeme;
    }

    public string goToBlockStatement(Block statement_) {
        StringBuilder builder = new StringBuilder();
        builder.Append("(block ");

        foreach (Statement statement in  statement_.statements) {
            builder.Append(statement.accept(this));
        }

        builder.Append(")");
        return builder.ToString();
    }

    public string goToExpressionStatement(Expression statement) {
        return parentSize(";", statement.expression);
    }

    public string goToFunctionStatement(Function statement) {
        StringBuilder builder = new StringBuilder();
        builder.Append("(funс ").Append(statement.name.lexeme).Append("(");

        foreach (Token parameter in statement.parameters) {
            if (parameter != statement.parameters[0]) builder.Append(" ");
            builder.Append(parameter.lexeme);
        }

        builder.Append(") ");

        foreach (Statement body in statement.body) {
            builder.Append(body.accept(this));
        }

        builder.Append(")");
        return builder.ToString();
    }

    public string goToIfStatement(If statement) {
        if (statement.elseBranch == null) {
            return parentSize2("if", statement.condition, statement.thenBranch);
        }

        return parentSize2("if-else", statement.condition, statement.thenBranch,
                statement.elseBranch);
    }

    public string goToPrintStatement(Print statement) {
        return parentSize("print", statement.expression);
    }

    public  string goToReturnStatement(Return statement) {
        if (statement.value == null) return "(return)";
        return parentSize("return", statement.value);
    }

    public string goToVarStatement(Var statement) {
        if (statement.initializer == null) {
            return parentSize2("var", statement.name);
        }

        return parentSize2("var", statement.name, "=", statement.initializer);
    }

    public string goToWhileStatement(While statement) {
        return parentSize2("while", statement.condition, statement.body);
    }

    private string parentSize(String name, params interp.Expression_[] expressions) {
        StringBuilder builder = new StringBuilder();

        builder.Append("(").Append(name);
        foreach (Expression_ expression in expressions) {
            builder.Append(" ");
            builder.Append(expression.accept(this));
        }
        builder.Append(")");

        return builder.ToString();
    }

    private  string parentSize2(String name, params Object[] parts) {
        StringBuilder builder = new StringBuilder();

        builder.Append("(").Append(name);

        foreach (Object part in parts) {
            builder.Append(" ");

            if (part is Expression_) {
                builder.Append(((Expression_)part).accept(this));
            } else if (part is Statement) {
                builder.Append(((Statement) part).accept(this));
            } else if (part is Token) {
                builder.Append(((Token) part).lexeme);
            } else {
                builder.Append(part);
            }
        }
        builder.Append(")");

        return builder.ToString();
    }
    }
}
