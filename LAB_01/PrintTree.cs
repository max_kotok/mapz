using System;
using System.Collections.Generic;
using System.Text;

namespace interp {

public class PrintTree : Expression_.Client<Node>, Statement.Client<Node> {

    public String print(Statement statement) {
        return statement.accept(this).toString();
    }

    Node Expression_.Client<Node>.visitAssignExpression(Assignment expression) {
        List<Node> childrens = new List<Node>();
        childrens.Add(new Node(expression.name.lexeme, new List<Node>()));
        childrens.Add(expression.value.accept(this));
        return new Node("=", childrens);
    }

     Node Expression_.Client<Node>.visitBinaryExpression(Binary expression) {
        List<Node> childrens = new List<Node>();
        childrens.Add(expression.right.accept(this));
        childrens.Add(expression.left.accept(this));
        return new Node(expression.operande.lexeme, childrens);
    }
     Node Expression_.Client<Node>.visitCallExpression(Call expression) {
        List<Node> childrens = new List<Node>();
        childrens.Add(expression.callIn.accept(this));
        List<Node> arguments = new List<Node>();
        foreach (Expression_ arg in expression.arguments) {
            arguments.Add(arg.accept(this));
        }
        childrens.Add(new Node("args", arguments));
        return new Node("call", childrens);
    }
     Node Expression_.Client<Node>.visitGroupingExpression(Grouping expressions) {
        List<Node> childrens = new List<Node>();
        childrens.Add(expressions.expression.accept(this));
        return new Node("group", childrens);
    }

     Node Expression_.Client<Node>.visitLiteralExpression(Literal expression) {
        if (expression.value == null) return new Node("nil", new List<Node>());
        return new Node(expression.value.ToString(), new List<Node>());
    }

     Node Expression_.Client<Node>.visitUnaryExpression(Unary expression) {
        List<Node> childs = new List<Node>();
        childs.Add(expression.right.accept(this));
        return new Node(expression.operande.lexeme, childs);
    }


     Node Statement.Client<Node>.goToBlockStatement(Block statements) {
        List<Node> childrens = new List<Node>();
        foreach (Statement statement in statements.statements) {
            if(statement == null)
            continue;
            childrens.Add(statement.accept(this));
        }
        return new Node("block", childrens);
    }

     Node Statement.Client<Node>.goToExpressionStatement(Expression statement) {
        List<Node> childrens = new List<Node>();
        childrens.Add(statement.expression.accept(this));
        return new Node(";", childrens);
    }

     Node Statement.Client<Node>.goToFunctionStatement(Function statement) {
        List<Node> parameters = new List<Node>();
        foreach (Token par in statement.parameters) {
            parameters.Add(new Node(par.lexeme, new List<Node>()));
        }
        List<Node> body = new List<Node>();
        foreach (Statement bodyStmt in statement.body) {
            body.Add(bodyStmt.accept(this));
        }
        List<Node> childrens = new List<Node>();
        childrens.Add(new Node("params", parameters));
        childrens.Add(new Node("body", body));
        return new Node("func "+statement.name.lexeme, childrens);
    }

     Node Statement.Client<Node>.goToIfStatement(If statement) {
        List<Node> childrens = new List<Node>();
        childrens.Add(statement.condition.accept(this));
        childrens.Add(statement.thenBranch.accept(this));
        if (statement.elseBranch != null) {
            childrens.Add(statement.elseBranch.accept(this));
            return new Node("if-else", childrens);
        }
        return new Node("if", childrens);
    }

     Node Statement.Client<Node>.goToPrintStatement(Print statement) {
        List<Node> childrens = new List<Node>();
        childrens.Add(statement.expression.accept(this));
        return new Node("print", childrens);
    }

     Node Statement.Client<Node>.goToReturnStatement(Return statements) {
        if (statements.value == null) return new Node("return", new List<Node>());
        List<Node> childrens = new List<Node>();
        childrens.Add(statements.value.accept(this));
        return new Node("return", childrens);
    }

     Node Statement.Client<Node>.goToVarStatement(Var statements) {
        List<Node> childrens = new List<Node>();
        if (statements.initializer == null) {
            childrens.Add(new Node(statements.name.lexeme, new List<Node>()));
        }
        else {
            List<Node> childrens_ = new List<Node>();
            childrens_.Add(new Node(statements.name.lexeme, new List<Node>()));
            childrens_.Add(statements.initializer.accept(this));
            childrens.Add(new Node("=", childrens_));
        }
        return new Node("var", childrens);
    }

     Node Statement.Client<Node>.goToWhileStatement(While statements) {
        List<Node> childrens = new List<Node>();
        childrens.Add(statements.condition.accept(this));
        childrens.Add(statements.body.accept(this));
        return new Node("while", childrens);
    }



        Node Expression_.Client<Node>.visitVariableExpression(Variable expression)
        {
            return new Node(expression.name.lexeme, new List<Node>());
        }
    }


}