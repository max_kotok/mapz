using System;
using System.Collections.Generic;

namespace interp
{
public abstract class Statement {

public abstract T accept<T>(Client<T> client);

  public interface Client<T> {
    T goToBlockStatement(Block statement);
    T goToExpressionStatement(Expression statement);
    T goToFunctionStatement(Function statement);
    T goToIfStatement(If statement);
    T goToPrintStatement(Print statementt);
    T goToReturnStatement(Return statement);
    T goToVarStatement(Var statement);
    T goToWhileStatement(While statement);
  }
}
public class Block : Statement {
    public Block(List<Statement> statements) {
      this.statements = statements;
    }

    public override T accept<T>(Client<T> client) {
      return client.goToBlockStatement(this);
    }
    public List<Statement> statements{ get; set; }
  }
public class Expression : Statement {
    public Expression(Expression_ expression) {
      this.expression = expression;
    }

    public override T accept<T>(Client<T> client) {
      return client.goToExpressionStatement(this);
    }

    public Expression_ expression{get; set; }
  }
   public class Function : Statement {
   public  Function(Token name, List<Token> parameter, List<Statement> body) {
      this.name = name;
      this.parameters = parameter;
      this.body = body;
    }

    public override T accept<T>(Client<T> client) {
      return client.goToFunctionStatement(this);
    }

     public Token name {get; set; }
     public List<Token> parameters {get; set; }
    public List<Statement> body {get; set; }
  }
  public class If : Statement {
    public If(Expression_ condition, Statement thenBranch, Statement elseBranch) {
      this.condition = condition;
      this.thenBranch = thenBranch;
      this.elseBranch = elseBranch;
    }

    public override T accept<T>(Client<T> client) {
      return client.goToIfStatement(this);
    }

     public Expression_ condition;
     public Statement thenBranch;
     public Statement elseBranch;
  }
public class Print : Statement {
    public Print(Expression_ expression) {
      this.expression = expression;
    }

    public override T accept<T>(Client<T> client) {

      return client.goToPrintStatement(this);
    }

    public Expression_ expression{get; set;}
  }
 public class Return : Statement {
    public Return(Token key, Expression_ value) {
      this.key = key;
      this.value = value;
    }

    public override T accept<T>(Client<T> client) {
      return client.goToReturnStatement(this);
    }

    public Token key{set;get;}
    public Expression_ value{set;get;}
  }
public class Var :  Statement {
    public Var(Token name, Expression_ initializer) {
      this.name = name;
      this.initializer = initializer;
    }
    public override T accept<T>(Client<T> client) {
      return client.goToVarStatement(this);
    }

    public Token name{set; get;}
     public Expression_ initializer{set;get;}
  }
   public class While : Statement {
    public While(Expression_ condition, Statement body) {
      this.condition = condition;
      this.body = body;
    }

    public override T accept<T>(Client<T> client) {
      return client.goToWhileStatement(this);
    }

    public Expression_ condition{set; get;}
    public Statement body{set; get;}
  }
}
