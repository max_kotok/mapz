using System;
using System.Collections.Generic;

namespace interp
{
    interface ICaller {
        int argumentNum();
        Object call(Interpreter interpreter, List<Object> arguments);
    }
}