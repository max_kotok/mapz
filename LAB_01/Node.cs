using System;
using System.Collections.Generic;
using System.Text;

namespace interp
{
    public class Node {

    String name;
    List<Node> children;

    public Node(String name, List<Node> children) {
        this.name = name;
        this.children = children;
    }

    public String toString() {
        StringBuilder buffer = new StringBuilder(50);
        print(buffer, "", "");
        return buffer.ToString();
    }

    private void print(StringBuilder buffer, String pref, String childPref) {
        buffer.Append(pref);
        buffer.Append(name);
        buffer.Append('\n');
        for (int i = 0; i < children.Count; i++) {
            Node next = children[i];
            if (i + 1 < children.Count) {
                next.print(buffer, childPref + "├── ", childPref + "│   ");
            } else {
                next.print(buffer, childPref + "└── ", childPref + "    ");
            }
        }
    }
}
}